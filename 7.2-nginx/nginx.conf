worker_processes auto;
pid          /var/run/nginx.pid;

events {
  worker_connections 1024;
}

http {
  include       /etc/nginx/mime.types;
  default_type  application/octet-stream;
  log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    '$status $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';
  access_log off;
  tcp_nopush     on;
  keepalive_timeout  65;
  gzip  on;
  gzip_comp_level 1;
  gzip_proxied any;
  gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript;

  server {
    root {WEBAPP_DIR}; ## &lt;-- Your only path $
    error_log /home/LogFiles/nginx/error.log;

    listen {PORT};
    listen [::]:{PORT};


    location = /favicon.ico {
      log_not_found off;
      access_log off;
    }

    location = /robots.txt {
      allow all;
      log_not_found off;
      access_log off;
    }

    location ~ \..*/.*\.php$ {
      return 403;
    }

    location ~ ^/sites/.*/private/ {
      return 403;
    }

    location ~* \.(?:ico|css|js|gif|jpe?g|png|woff|ttf|mp4|svg|csv|xls|xlsx|woff2|pdf)$ {
      expires max;
      add_header Pragma public;
      add_header Cache-Control "public, must-revalidate, proxy-revalidate";
    }

    # Block access to "hidden" files and directories whose names begin with a
    # period. This includes directories used by version control systems such
    # as Subversion or Git to store control files.
    location ~ (^|/)\. {
      return 403;
    }

    location / {
      # try_files $uri @rewrite; # For Drupal &lt;= 6
      try_files $uri /index.php?$query_string; # For Drupal &gt;= 7
    }

    location @rewrite {
      rewrite ^/(.*)$ /index.php?q=$1;
    }

    # In Drupal 8, we must also match new paths where the '.php' appears in the middle,
    # such as update.php/selection. The rule we use is strict, and only allows this pattern
    # with the update.php front controller.  This allows legacy path aliases in the form of
    # blog/index.php/legacy-path to continue to route to Drupal nodes. If you do not have
    # any paths like that, then you might prefer to use a laxer rule, such as:
    #   location ~ \.php(/|$) {
      # The laxer rule will continue to work if Drupal uses this new URL pattern with front
      # controllers other than update.php in a future release.
      location ~ '\.php$|^/update.php' {
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_split_path_info ^(.+?\.php)(/.*)?$;
        fastcgi_connect_timeout         300;
        fastcgi_send_timeout           3600;
        fastcgi_read_timeout           3600;
        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;
        fastcgi_temp_file_write_size 256k;
        fastcgi_intercept_errors on;
        fastcgi_index index.php;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PHP_VALUE "upload_max_filesize=1024M \n post_max_size=1024M \n max_input_vars=100000 \n memory_limit=1024M \n max_execution_time=600 \n allow_url_fopen=On \n allow_url_include=Off";
        include fastcgi_params;
      }

      # Fighting with Styles? This little gem is amazing.
      # location ~ ^/sites/.*/files/imagecache/ { # For Drupal &lt;= 6
        location ~ ^/sites/.*/files/styles/ { # For Drpal &gt;= 7
          try_files $uri @rewrite;
        }

        location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
          expires max;
          log_not_found off;
        }
      }
    }


